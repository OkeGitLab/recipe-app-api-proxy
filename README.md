# Recipe api app proxy

NGINX proxy app for recipe api app

## Usage


### Environmental variables

  * 'LISTEN_PORT' - Port to listen on (default: '8000')
  * 'APP_HOST' - Hostname of the app to forward request to (default: 'app')
  * 'APP_PORT' - Port of the app top forward request to (default: '9000')



